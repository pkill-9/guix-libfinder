#/bin/sh

get_package_name_from_store_path () {
    echo $1 | sed -re 's/\/gnu\/store\/[0-9a-z]+-//' \
                  -e 's/\/.+//' \
                  -e 's/([a-z0-9\-\+]+)-([0-9\.]+)-([a-z]+)/\1:\3/' \
                  -e 's/([a-z0-9\-\+]+)-([0-9\.]+)/\1/'
}

if [ "$1" == "find-all-so-files" ] ; then
# generate list of every .so file in the store
    find /gnu/store -type f -name '*.so*'
elif [ "$1" == "generate-list-of-packages-and-so-files" ] ; then
    while read data; do
	package_name=$(get_package_name_from_store_path $data)
	so_file=$(basename $data)
	echo $package_name $so_file
    done
else
    echo """Usage:
"find-all-so-files"
"generate-list-of-packages-and-so-files"
"""
fi
